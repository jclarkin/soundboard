![Build Status](https://gitlab.com/jclarkin/soundboard/badges/master/build.svg)

---

Simple web Soundboard for my kids to play with.

[Click Here to play](https://jclarkin.gitlab.io/soundboard/)
